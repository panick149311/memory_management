#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

void FIFO(int *, int*, int, int);
void Optimal(int *, int *, int, int);
void LRU(int *, int *, int, int);
void init_page_frame(int *, int);
void init_reference_string(int *, int);
void print_reference_string(int *, int);
void print_page_frames(int *, int , bool);

void main() {
	srand(time(0));
	int rs_size;
	int pf_size = 0;
	cout << "Input size of reference string: ";
	cin >> rs_size;
	cout << "Input size of page frames: ";
	cin >> pf_size;
	int *reference_string = new int[rs_size];
	int *page_frames = new int[pf_size];


	init_reference_string(reference_string, rs_size);
	print_reference_string(reference_string, rs_size);
	
	init_page_frame(page_frames, pf_size);
	FIFO(reference_string, page_frames, rs_size, pf_size);

	init_page_frame(page_frames, pf_size);
	Optimal(reference_string, page_frames, rs_size, pf_size);

	init_page_frame(page_frames, pf_size);
	LRU(reference_string, page_frames, rs_size, pf_size);
	system("pause");
}


void init_page_frame(int *page_frames, int pf_size) {
	for (int i = 0; i < pf_size; i++) {
		page_frames[i] = -1;
	}
}

void init_reference_string(int *reference_string, int rs_size) {
	for (int i = 0; i < rs_size; i++) {
		reference_string[i] = rand() % 16;
	}
}

void print_reference_string(int *reference_string, int rs_size) {
	for (int j = 0; j < rs_size; j++) {
		cout << "[" << reference_string[j] << "] ";
	}
	cout << endl;
}

void print_page_frames(int *page_frames, int pf_size,bool page_fault_occur) {
	for (int j = 0; j < pf_size; j++) {
		if (page_frames[j] == -1) {
			cout << setw(7) << "[  ]";
		}
		else if (page_frames[j] < 10){
			cout << setw(5) << "[ " << page_frames[j] << "]";
		}
		else {
			cout << setw(4) << "[" << page_frames[j] << "]";
		}
	}
	if (page_fault_occur) {
		cout << setw(15) << "Page fault";
	}
	cout << endl;
}

void FIFO(int *reference_string, int *page_frames, int rs_size, int pf_size) {
	int page_faults = 0;
	int current_page = 0;
	bool page_fault_occur;
	bool success;
	cout << "-----------------------------------------------" << endl;
	cout << "First in first out algorithm" << endl;
	cout << "-----------------------------------------------" << endl;
	cout << "Page frames" << endl;
	for (int i = 0; i < rs_size; i++) {
		success = false;
		page_fault_occur = false;
		for (int j = 0; j < pf_size; j++) {
			if (reference_string[i] == page_frames[j]) {
				success = true;
				break;
			}
		}
		if (!success) {
			for (int j = 0; j < pf_size; j++) {
				if (page_frames[j] == -1) {
					page_frames[j] = reference_string[i];
					page_faults++;
					page_fault_occur = true;
					success = true;
					break;
				}
			}
		}
		if (!success) {
			page_frames[current_page] = reference_string[i];
			current_page++;
			page_faults++;
			page_fault_occur = true;
			if (current_page == pf_size) current_page = 0;
		}
		print_page_frames(page_frames, pf_size ,page_fault_occur);
	}
	cout << "Page faults = " << page_faults << endl;
}


void Optimal(int *reference_string, int *page_frames, int rs_size, int pf_size) {
	int page_faults = 0;
	bool success;
	bool page_fault_occur;
	cout << "-----------------------------------------------" << endl;
	cout << "Optimal algorithm" << endl;
	cout << "-----------------------------------------------" << endl;
	cout << "Page frames" << endl;
	for (int i = 0; i < rs_size; i++) {
		success = false;
		page_fault_occur = false;
		for (int j = 0; j < pf_size; j++) {
			if (reference_string[i] == page_frames[j]) {
				success = true;
				break;
			}
		}
		if (!success) {
			for (int j = 0; j < pf_size; j++) {
				if (page_frames[j] == -1) {
					page_frames[j] = reference_string[i];
					page_faults++;
					page_fault_occur = true;
					success = true;
					break;
				}
			}
		}
		if (!success) {
			int period_time = 0;
			int longest_pt = 0;
			int longest_pt_index = 0;
			bool found;
			for (int j = 0; j < pf_size; j++) {
				found = false;
				for (int k = i + 1; k < rs_size; k++) {
					if (page_frames[j] == reference_string[k]) {
						period_time = k;
						if (period_time > longest_pt) {
							longest_pt = period_time;
							longest_pt_index = j;
						}
						found = true;
						break;
					}
				}
				if (!found) {
					longest_pt = rs_size + 1;
					longest_pt_index = j;
					break;
				}
			}
			page_frames[longest_pt_index] = reference_string[i];
			page_faults++;
			page_fault_occur = true;
		}
		print_page_frames(page_frames, pf_size,page_fault_occur);
	}
	cout << "Page faults = " << page_faults << endl;
}

void LRU(int *reference_string, int *page_frames, int rs_size, int pf_size) {
	int page_faults = 0;
	bool success;
	bool page_fault_occur;
	cout << "-----------------------------------------------" << endl;
	cout << "Last recently use algorithm" << endl;
	cout << "-----------------------------------------------" << endl;
	cout << "Page frames" << endl;
	for (int i = 0; i < rs_size; i++) {
		success = false;
		page_fault_occur = false;
		for (int j = 0; j < pf_size; j++) {
			if (reference_string[i] == page_frames[j]) {
				success = true;
				break;
			}
		}
		if (!success) {
			for (int j = 0; j < pf_size; j++) {
				if (page_frames[j] == -1) {
					page_frames[j] = reference_string[i];
					page_faults++;
					page_fault_occur = true;
					success = true;
					break;
				}
			}
		}
		if (!success) {
			int amount_of_time = 0;
			int most_aof = 0;
			int LRU_index;
			for (int j = 0; j < pf_size; j++) {
				for (int k = i - 1; k > -1; k--) {
					if (page_frames[j] == reference_string[k]) {
						amount_of_time = i - k;
						if (amount_of_time > most_aof) {
							LRU_index = j;
							most_aof = amount_of_time;
						}
						break;
					}
					
				}
			}
			page_frames[LRU_index] = reference_string[i];
			page_faults++;
			page_fault_occur = true;
		}
		print_page_frames(page_frames, pf_size,page_fault_occur);
	}
	cout << "Page faults = " << page_faults << endl;
}
